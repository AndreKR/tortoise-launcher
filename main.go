package main

// This doesn't work with go generate, but compile-icon.bat works:
//go:generate rsrc -arch="amd64" -ico="res\\icon.ico"

import (
	"encoding/json"
	"github.com/lxn/win"
	"github.com/shibukawa/glfw"
	"github.com/shibukawa/nanogui-go"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"unsafe"
)

type FileStructure struct {
	AlwaysOnTop       bool         `json:"always_on_top"`
	MinimizeOnClick   bool         `json:"minimize_on_click"`
	ShowPush          bool         `json:"show_push"`
	ShowLogAfterFetch bool         `json:"show_log_after_fetch"`
	Repositories      []Repository `json:"repositories"`
}

type Repository struct {
	Name   string `json:"name"`
	Hidden bool   `json:"hidden"`
	Path   string `json:"path"`
}

var repositories []Repository
var editMode bool
var alwaysOnTop bool
var minimizeOnClick bool
var showPush bool
var showLogAfterFetch bool
var mostRecentlyUsedRepository = -1
var globalLock sync.Mutex

type Application struct {
	mainWindow *nanogui.Screen
	shader     *nanogui.GLShader
}

func (a *Application) init() {
	glfw.WindowHint(glfw.Samples, 4)
	glfw.WindowHint(glfw.PreferLowPowerToHighPerformance, 1)

	a.mainWindow = nanogui.NewScreen(300, 400, "Tortoise Launcher", true, false)

	hwnd := win.HWND(unsafe.Pointer(a.mainWindow.GLFWWindow().GetWin32Window()))

	win.SetWindowLong(hwnd, win.GWL_STYLE, win.GetWindowLong(hwnd, win.GWL_STYLE)&^win.WS_MAXIMIZEBOX)

	//a.mainWindow.SetTheme(nanogui.NewStandardTheme(a.mainWindow.NVGContext()))
}

func main() {

	loadConfig()

	nanogui.Init()
	//nanogui.SetDebug(true)
	app := Application{}
	app.init()
	renderMainWindow(app.mainWindow)

	app.mainWindow.DrawAll()
	app.mainWindow.SetVisible(true)
	applyAlwaysOnTop(app.mainWindow)

	hwnd := win.HWND(unsafe.Pointer(app.mainWindow.GLFWWindow().GetWin32Window()))

	var iconGroupOrdinal uintptr = 7

	// Load the icon from the exe file
	smallIcon := win.LoadImage(win.GetModuleHandle(nil), win.MAKEINTRESOURCE(iconGroupOrdinal), win.IMAGE_ICON, win.GetSystemMetrics(win.SM_CXSMICON), win.GetSystemMetrics(win.SM_CYSMICON), win.LR_SHARED)
	bigIcon := win.LoadImage(win.GetModuleHandle(nil), win.MAKEINTRESOURCE(iconGroupOrdinal), win.IMAGE_ICON, win.GetSystemMetrics(win.SM_CXICON), win.GetSystemMetrics(win.SM_CYICON), win.LR_SHARED)
	// Set the icon as the icon for the window of the running process
	win.SendMessage(hwnd, win.WM_SETICON, 0, uintptr(smallIcon))
	win.SendMessage(hwnd, win.WM_SETICON, 1, uintptr(bigIcon))

	// TODO: nanogui uses github.com/shibukawa/glfw-2, a fork of github.com/go-gl/glfw. It only contains GLFW 3.2, which
	//       does not support GetWorkarea(). If GLFW ever gets updated to 3.3, we should restore the window position here,
	//       but only after checking if the old position is still within the work area of any monitor.

	nanogui.MainLoop()
}

func applyAlwaysOnTop(screen *nanogui.Screen) {
	globalLock.Lock()
	aot := alwaysOnTop
	globalLock.Unlock()

	hwnd := win.HWND(unsafe.Pointer(screen.GLFWWindow().GetWin32Window()))
	if aot {
		win.SetWindowPos(hwnd, win.HWND_TOPMOST, 0, 0, 0, 0, win.SWP_NOMOVE|win.SWP_NOSIZE)
	} else {
		win.SetWindowPos(hwnd, win.HWND_NOTOPMOST, 0, 0, 0, 0, win.SWP_NOMOVE|win.SWP_NOSIZE)
	}
}

func loadConfig() {
	data := FileStructure{}

	appdataDir, _ := os.LookupEnv("APPDATA")
	configFile := filepath.Join(appdataDir, "TortoiseLauncher", "config.json")

	configFileData, err := ioutil.ReadFile(configFile)
	if err != nil {
		return
	}

	_ = json.Unmarshal(configFileData, &data)

	globalLock.Lock()
	alwaysOnTop = data.AlwaysOnTop
	minimizeOnClick = data.MinimizeOnClick
	showPush = data.ShowPush
	showLogAfterFetch = data.ShowLogAfterFetch
	repositories = data.Repositories
	globalLock.Unlock()
}

func persistConfig() {
	data := FileStructure{}

	globalLock.Lock()
	data.AlwaysOnTop = alwaysOnTop
	data.MinimizeOnClick = minimizeOnClick
	data.ShowPush = showPush
	data.ShowLogAfterFetch = showLogAfterFetch
	data.Repositories = repositories
	globalLock.Unlock()

	appdataDir, _ := os.LookupEnv("APPDATA")
	configFile := filepath.Join(appdataDir, "TortoiseLauncher", "config.json")

	configFileData, err := json.Marshal(data)
	if err != nil {
		return
	}

	_ = os.MkdirAll(filepath.Dir(configFile), 0755)
	err = ioutil.WriteFile(configFile, configFileData, 0644)
	if err != nil {
		_ = os.Remove(configFile)
	}
}
