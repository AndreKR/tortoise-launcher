package main

import (
	"github.com/harry1453/go-common-file-dialog/cfd"
	"github.com/harry1453/go-common-file-dialog/cfdutil"
	"github.com/shibukawa/glfw"
	"github.com/shibukawa/nanogui-go"
	"github.com/shibukawa/nanovgo"
	"golang.org/x/sys/windows/registry"
	"os"
	"os/exec"
	"path/filepath"
	"syscall"
)

const projectNameFontSize = 18
const projectButtonFontSize = 17
const projectButtonWidth = 65
const helperFontSize = 14

var tortoiseGitProc = findTortoiseGitProc()

func renderMainWindow(screen *nanogui.Screen) {

	globalLock.Lock()
	defer globalLock.Unlock()

	clear(screen)

	screen.SetBackgroundColor(nanovgo.MONO(45, 0))

	windowLayout := nanogui.NewBoxLayout(nanogui.Vertical, nanogui.Fill, 10)
	screen.SetLayout(windowLayout)
	var suppressLayout bool
	screen.GLFWWindow().SetIconifyCallback(func(w *glfw.Window, iconified bool) {
		suppressLayout = true
	})
	screen.SetResizeEventCallback(func(newW, newH int) bool {
		// For everything to work properly (no missing elements) we have to run PerformLayout when this callback is called
		// on application start, but we must not run it when this callback is called when restoring the minimized window. :/
		if !suppressLayout {
			screen.PerformLayout()
		}
		suppressLayout = false
		if newW < 0 || newH < 0 { // this happens when restoring a minified window
			return true // this has no meaning, it will be discarded
		}
		checkWindowSize(screen, windowLayout)
		screen.DrawAll()
		return true // this has no meaning, it will be discarded
	})

	caption := "Edit mode"

	if editMode {
		renderEditMode(screen, screen)
		caption = "Save and exit edit mode"
	} else {
		renderProjectList(screen, screen, false)

		nanogui.NewWidget(screen).SetHeight(10)

		checkboxBar := nanogui.NewWidget(screen)
		checkboxBar.SetLayout(nanogui.NewBoxLayout(nanogui.Horizontal, nanogui.Minimum, 0, 20))

		c := nanogui.NewCheckBox(checkboxBar, "Show hidden repositories")
		c.SetFontSize(helperFontSize)
		hiddenBox := nanogui.NewWidget(screen)
		hiddenBox.SetLayout(nanogui.NewBoxLayout(nanogui.Vertical, nanogui.Fill))
		hiddenBox.SetVisible(false)
		c.SetCallback(func(checked bool) {
			hiddenBox.SetVisible(checked)
			screen.PerformLayout()
			checkWindowSize(screen, windowLayout)
		})
		renderProjectList(screen, hiddenBox, true)

		c = nanogui.NewCheckBox(checkboxBar, "Show Push")
		c.SetFontSize(helperFontSize)
		c.SetChecked(showPush)
		c.SetCallback(func(checked bool) {
			globalLock.Lock()
			showPush = checked
			globalLock.Unlock()
			persistConfig()
			renderMainWindow(screen)
		})

		c = nanogui.NewCheckBox(checkboxBar, "Show Log after Fetch")
		c.SetFontSize(helperFontSize)
		c.SetChecked(showLogAfterFetch)
		c.SetCallback(func(checked bool) {
			globalLock.Lock()
			showLogAfterFetch = checked
			globalLock.Unlock()
			persistConfig()
			renderMainWindow(screen)
		})
	}

	nanogui.NewWidget(screen).SetHeight(15)

	settingsBar := nanogui.NewWidget(screen)
	settingsBar.SetLayout(nanogui.NewBoxLayout(nanogui.Horizontal, nanogui.Minimum, 0, 20))

	var c *nanogui.CheckBox
	c = nanogui.NewCheckBox(settingsBar, "Always on top")
	c.SetFontSize(helperFontSize)
	c.SetChecked(alwaysOnTop)
	c.SetCallback(func(checked bool) {
		globalLock.Lock()
		alwaysOnTop = checked
		globalLock.Unlock()
		persistConfig()
		applyAlwaysOnTop(screen)
	})

	c = nanogui.NewCheckBox(settingsBar, "Minimize on click")
	c.SetFontSize(helperFontSize)
	c.SetChecked(minimizeOnClick)
	c.SetCallback(func(checked bool) {
		globalLock.Lock()
		minimizeOnClick = checked
		globalLock.Unlock()
		persistConfig()
	})

	c = nanogui.NewCheckBox(settingsBar, "Run on startup")
	c.SetFontSize(helperFontSize)
	runOnStartup := false
	// I hope this isn't slow :)
	k, err := registry.OpenKey(registry.CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", registry.QUERY_VALUE)
	if err == nil {
		_, _, errNotExist := k.GetStringValue("TortoiseLauncher")
		if errNotExist == nil {
			runOnStartup = true
		}
	}
	c.SetChecked(runOnStartup)
	c.SetCallback(func(checked bool) {
		if checked {
			k, err := registry.OpenKey(registry.CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", registry.SET_VALUE)
			if err != nil {
				return
			}
			_ = k.SetStringValue("TortoiseLauncher", os.Args[0])
		} else {
			k, err := registry.OpenKey(registry.CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", registry.SET_VALUE)
			if err != nil {
				return
			}
			_ = k.DeleteValue("TortoiseLauncher")
		}

	})

	nanogui.NewWidget(screen).SetHeight(15)

	b := nanogui.NewButton(screen, caption)
	b.SetFontSize(helperFontSize)
	b.SetCallback(func() {
		globalLock.Lock()
		editMode = !editMode
		globalLock.Unlock()
		if !editMode {
			// we just exited edit mode, persist repository list
			persistConfig()
		}
		renderMainWindow(screen)
	})

	screen.PerformLayout()
	checkWindowSize(screen, windowLayout)
}

func renderProjectList(screen *nanogui.Screen, parent nanogui.Widget, hidden bool) {
	horSpacing := 2
	verSpacing := 2

	//mostRecentlyUsedColor := nanovgo.HSL(48.0 / 360, 70.0 / 100, 90.0 / 200)
	mostRecentlyUsedColor := nanovgo.MONO(255, 230)

	projectList := nanogui.NewWidget(parent)
	columns := []int{0, 10, 0, horSpacing, 0, horSpacing, 0, horSpacing, 0}
	if showPush {
		columns = append(columns, horSpacing, 0)
	}
	listLayout := nanogui.NewAdvancedGridLayout(columns)
	listLayout.SetColStretch(0, 1)
	projectList.SetLayout(listLayout)
	row := -1
	for n, repo := range repositories {
		if repo.Hidden != hidden {
			continue
		}
		row++

		repoPath := repo.Path
		idx := n

		// Row with project Name and main buttons
		listLayout.AppendRow(0)
		var l *nanogui.Label
		l = nanogui.NewLabel(projectList, repo.Name)
		l.SetFontSize(projectNameFontSize)
		if idx == mostRecentlyUsedRepository {
			l.SetColor(mostRecentlyUsedColor)
		}
		listLayout.SetAnchor(l, nanogui.NewAnchor(0, row*2))
		var b *nanogui.Button
		b = nanogui.NewButton(projectList, "")
		b.SetIcon('📁')
		b.SetFontSize(projectButtonFontSize)
		if idx == mostRecentlyUsedRepository {
			b.SetTextColor(mostRecentlyUsedColor)
		}
		b.SetCallback(func() {
			_ = exec.Command("explorer", repoPath).Start()
			if minimizeOnClick {
				_ = screen.GLFWWindow().Iconify()
			}
			globalLock.Lock()
			mostRecentlyUsedRepository = idx
			globalLock.Unlock()
			renderMainWindow(screen)
		})
		listLayout.SetAnchor(b, nanogui.NewAnchor(2, row*2))
		b = nanogui.NewButton(projectList, "Fetch")
		b.SetFontSize(projectButtonFontSize)
		b.SetFixedWidth(projectButtonWidth)
		if idx == mostRecentlyUsedRepository {
			b.SetTextColor(mostRecentlyUsedColor)
		}
		b.SetCallback(func() {
			if showLogAfterFetch {
				startFetchAndLog(repoPath)
			} else {
				startTortoiseGit("fetch", repoPath)
			}
			if minimizeOnClick {
				_ = screen.GLFWWindow().Iconify()
			}
			globalLock.Lock()
			mostRecentlyUsedRepository = idx
			globalLock.Unlock()
			renderMainWindow(screen)
		})
		listLayout.SetAnchor(b, nanogui.NewAnchor(4, row*2))
		b = nanogui.NewButton(projectList, "Log")
		b.SetFontSize(projectButtonFontSize)
		b.SetFixedWidth(projectButtonWidth)
		if idx == mostRecentlyUsedRepository {
			b.SetTextColor(mostRecentlyUsedColor)
		}
		b.SetCallback(func() {
			startTortoiseGit("log", repoPath)
			if minimizeOnClick {
				_ = screen.GLFWWindow().Iconify()
			}
			globalLock.Lock()
			mostRecentlyUsedRepository = idx
			globalLock.Unlock()
			renderMainWindow(screen)
		})
		listLayout.SetAnchor(b, nanogui.NewAnchor(6, row*2))
		b = nanogui.NewButton(projectList, "Commit")
		b.SetFontSize(projectButtonFontSize)
		b.SetFixedWidth(projectButtonWidth)
		if idx == mostRecentlyUsedRepository {
			b.SetTextColor(mostRecentlyUsedColor)
		}
		b.SetCallback(func() {
			startTortoiseGit("commit", repoPath)
			if minimizeOnClick {
				_ = screen.GLFWWindow().Iconify()
			}
			globalLock.Lock()
			mostRecentlyUsedRepository = idx
			globalLock.Unlock()
			renderMainWindow(screen)
		})
		listLayout.SetAnchor(b, nanogui.NewAnchor(8, row*2))
		if showPush {
			b = nanogui.NewButton(projectList, "Push")
			b.SetFontSize(projectButtonFontSize)
			b.SetFixedWidth(projectButtonWidth)
			if idx == mostRecentlyUsedRepository {
				b.SetTextColor(mostRecentlyUsedColor)
			}
			b.SetCallback(func() {
				startTortoiseGit("push", repoPath)
				if minimizeOnClick {
					_ = screen.GLFWWindow().Iconify()
				}
				globalLock.Lock()
				mostRecentlyUsedRepository = idx
				globalLock.Unlock()
				renderMainWindow(screen)
			})
			listLayout.SetAnchor(b, nanogui.NewAnchor(10, row*2))
		}

		// Spacer row
		listLayout.AppendRow(verSpacing)
	}
}

func renderEditMode(screen *nanogui.Screen, parent nanogui.Widget) {
	horSpacing := 2
	verSpacing := 2

	projectList := nanogui.NewWidget(parent)
	listLayout := nanogui.NewAdvancedGridLayout([]int{0, 2, 0, 0, 0, horSpacing, 0, horSpacing, 0})
	listLayout.SetColStretch(0, 1)
	projectList.SetLayout(listLayout)
	for n, repo := range repositories {
		idx := n

		// Row with project Name and various edit buttons
		listLayout.AppendRow(0)
		e := nanogui.NewTextBox(projectList, repo.Name)
		e.SetFontSize(projectNameFontSize)
		e.SetEditable(true)
		e.SetAlignment(nanogui.TextLeft)
		listLayout.SetAnchor(e, nanogui.NewAnchor(0, idx*2))
		e.SetCallback(func(newName string) bool {
			globalLock.Lock()
			repositories[idx].Name = newName
			globalLock.Unlock()
			return true
		})
		var b *nanogui.Button

		// Up/down buttons
		b = nanogui.NewButton(projectList, "")
		b.SetIcon('⬆')
		b.SetFontSize(16)
		if idx == 0 {
			disableButton(b)
		} else {
			b.SetCallback(func() {
				globalLock.Lock()
				repositories[idx-1], repositories[idx] = repositories[idx], repositories[idx-1]
				globalLock.Unlock()
				renderMainWindow(screen)
			})
		}
		listLayout.SetAnchor(b, nanogui.NewAnchor(2, idx*2))

		b = nanogui.NewButton(projectList, "")
		b.SetIcon('⬇')
		b.SetFontSize(16)
		if idx == len(repositories)-1 {
			disableButton(b)
		} else {
			b.SetCallback(func() {
				globalLock.Lock()
				repositories[idx+1], repositories[idx] = repositories[idx], repositories[idx+1]
				globalLock.Unlock()
				renderMainWindow(screen)
			})
		}
		listLayout.SetAnchor(b, nanogui.NewAnchor(4, idx*2))

		// Show/hide button

		caption := "Hide"
		if repo.Hidden {
			caption = "Show"
		}
		b = nanogui.NewButton(projectList, caption)
		b.SetFontSize(helperFontSize)
		b.SetFixedWidth(48)
		b.SetCallback(func() {
			globalLock.Lock()
			repositories[idx].Hidden = !repositories[idx].Hidden
			globalLock.Unlock()
			renderMainWindow(screen)
		})
		listLayout.SetAnchor(b, nanogui.NewAnchor(6, idx*2))

		// Remove button
		b = nanogui.NewButton(projectList, "")
		removeIcons := []rune("✕✖❎➖⊟")
		b.SetIcon(nanogui.Icon(removeIcons[idx%5]))
		b.SetFontSize(16)
		b.SetCallback(func() {
			globalLock.Lock()
			repositories = append(repositories[:idx], repositories[idx+1:]...)
			globalLock.Unlock()
			renderMainWindow(screen)
		})
		listLayout.SetAnchor(b, nanogui.NewAnchor(8, idx*2))

		// Spacer row
		listLayout.AppendRow(verSpacing)
	}

	addButton := nanogui.NewButton(screen, "Add repository")
	addButton.SetFontSize(helperFontSize)
	addButton.SetCallback(func() {
		folder, err := cfdutil.ShowPickFolderDialog(cfd.DialogConfig{
			Title: "Pick Repository",
			Role:  "AddRepository",
		})

		if err == nil {
			globalLock.Lock()
			repositories = append(repositories, Repository{
				Name:   filepath.Base(folder),
				Hidden: false,
				Path:   folder,
			})
			globalLock.Unlock()
			persistConfig()
			renderMainWindow(screen)
		}
	})
}

func checkWindowSize(screen *nanogui.Screen, windowLayout *nanogui.BoxLayout) {
	// Do a layout with the new size
	screen.PerformLayout()
	// Then check if the content wants to be larger
	prefW, prefH := windowLayout.PreferredSize(screen, screen.NVGContext())
	windowW := screen.Width()
	windowH := screen.Height()
	if prefW > windowW || prefH > windowH {
		// If so, adjust the new sizes
		if prefW > windowW {
			windowW = prefW
		}
		if prefH > windowH {
			windowH = prefH
		}
		// Then undo the resize by resizing again
		// This flickers, but it's currently my best solution because shibukawa/glfw-2 does not expose
		// glfwSetWindowSizeLimits from window.c via window.go
		screen.GLFWWindow().SetSize(windowW, windowH)
	}
}

func clear(screen *nanogui.Screen) {
	for screen.ChildCount() > 0 {
		screen.RemoveChildByIndex(0)
	}
}

func disableButton(b *nanogui.Button) {
	b.SetFlags(nanogui.ToggleButtonType)
	b.SetPushed(true) // this kind of looks like disabled
	b.SetChangeCallback(func(_ bool) {
		b.SetPushed(true) // keep them that way
	})
}

func startFetchAndLog(path string) {
	go func() {
		c := exec.Command(tortoiseGitProc)
		c.SysProcAttr = &syscall.SysProcAttr{CmdLine: tortoiseGitProc + ` /command:fetch /path:"` + path + `"`}
		_ = c.Run()
		c = exec.Command(tortoiseGitProc)
		c.SysProcAttr = &syscall.SysProcAttr{CmdLine: tortoiseGitProc + ` /command:log /path:"` + path + `"`}
		_ = c.Start()
	}()
}

func startTortoiseGit(command, path string) {
	c := exec.Command(tortoiseGitProc)
	c.SysProcAttr = &syscall.SysProcAttr{CmdLine: tortoiseGitProc + ` /command:` + command + ` /path:"` + path + `"`}
	_ = c.Start()
}

func findTortoiseGitProc() string {
	k, err := registry.OpenKey(registry.LOCAL_MACHINE, "SOFTWARE\\TortoiseGit", registry.QUERY_VALUE)
	if err != nil {
		return ""
	}
	p, _, err := k.GetStringValue("ProcPath")
	if err != nil {
		return ""
	}
	return p
}
