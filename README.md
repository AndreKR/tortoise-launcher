# Tortoise Launcher

![](docs/logo.png)

Keeps a list of repositories and launches the TortoiseGit Fetch, Log and Commit dialogs in them.

![](docs/screenshot.png)

## Usage

[Download](https://gitlab.com/api/v4/projects/16924243/jobs/artifacts/master/raw/tortoise-launcher.exe?job=build) and start